import Map from './map'

const routes = [
  Map
]

export default routes
export let navs = createNavs(routes)


// helper
function createNavs(routes) {
  return routes.map((item) => {
    let nav = {
        name: item.name,
        path: item.path,
        icon: item.icon,
        text: item.text,
    }
    if (item.children) {
      nav.children = createNavs(item.children)
      delete nav.path
    }
    return nav
  })
}
