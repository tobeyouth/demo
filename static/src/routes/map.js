import MapView from 'views/map'

const route = {
  path: '/',
  name: 'map',
  text: 'map',
  component: MapView
}

export default route