import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'

let creator

const sagaMiddleware = createSagaMiddleware()

if (process.env.NODE_ENV === 'production') {
  creator = compose(
    applyMiddleware(sagaMiddleware)
  )(createStore)
} else {
  creator = compose(
    applyMiddleware(sagaMiddleware),
    applyMiddleware(logger)
  )(createStore)
}

export default function configureStore(reducer, sagaWatcher, initState) {
  const store = creator(reducer, initState)
  if (sagaWatcher) {
    sagaMiddleware.run(sagaWatcher)
  }
  return store
}