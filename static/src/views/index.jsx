import './style.scss'

import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import classnames from 'classnames'
import routes, { navs } from 'routes'
import { renderRoutes } from 'react-router-config'


class App extends Component {

  constructor (props) {
    super(props)
  }

  render () {
    return (
      <Router basename='/'>
        <div className="app-container">
          <div className="app-body">
            { renderRoutes(routes) }
          </div>
        </div>
      </Router>
    )
  }
}

export default App