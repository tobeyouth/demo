import './style.scss'
import 'leaflet/dist/leaflet.css'
import React, { Component } from 'react'
import classnames from 'classnames'
import { Map, TileLayer, Marker, Popup, WMSTileLayer,
         Polygon, Polyline } from 'react-leaflet'
import { latLngBounds, latLng, CRS, FitBoundsOptions } from 'leaflet'
import { MAPBOX_API, MAPBOX_TOKEN } from 'constants/const'
import { POLYGON_POSITIONS, POLYLINE_POSITIONS, 
         FUTURE_POLYGON_POSITIONS, FUTURE_POLYLINE_POSITIONS,
         LAGLNGS } from 'constants/mocks'


const KITS = [
  {
    text: '核心商业区',
    type: 'polygons',
    name: 'commercial'
  },
  {
    text: '修建中道路',
    type: 'polylines',
    name: 'road'
  },
  {
    text: '规划核心商业区',
    type: 'polygons',
    name: 'future-commercial'
  },
  {
    text: '规划道路',
    type: 'polygons',
    name: 'future-road'
  }
]


class MapView extends Component {

  fetchData = (type, name) => {
    let data = {}

    if (name === 'commercial') {
      data = {
        positions: POLYGON_POSITIONS,
        color: '#3388ff'
      }
    } else if (name === 'road') {
      data = {
        positions: POLYLINE_POSITIONS,
        color: '#3388ff'
      }
    } else if (name === 'future-commercial') {
      data = {
        positions: FUTURE_POLYGON_POSITIONS,
        color: 'red'
      }
    } else if (name === 'future-road') {
      data = {
        positions: FUTURE_POLYLINE_POSITIONS,
        color: 'red'
      }
    }
    this._updateState(type, name, data)
  }

  _updateState = (type, name, data) => {
    
    let state = {}
    state[type] = [...this.state[type]]
    
    let existed = state[type].find((item) => {
      return item.name === name
    })
    if (existed) {
      state[type] = state[type].filter((item) => {
        return item.name !== name
      })
    } else {
      state[type] = state[type].concat({
        type,
        name,
        data
      })
    }

    this.setState(state)
  }

  constructor (props) {
    super(props)
    this.state = {
      // map options
      // lat: 51.505,
      // lng: -0.09,
      lat: LAGLNGS[0],
      lng: LAGLNGS[1],
      zoom: 13,
      
      // markers
      markers: [],
      // circles
      circles: [],
      // polygons
      polygons: [],
      // polylines
      polylines: [],

      selectedSource: []
    }
  }

  render () {
    const { lat, lng, zoom,
            polygons,  polylines} = this.state
    const center = [lat, lng]

    return (
      <div className='view-map'>
        <Map center={ center }
          zoom={ zoom }>
          <TileLayer url={ MAPBOX_API }
            id={ 'mapbox.streets' }
            width={ 256 }
            height={ 256 }
            accessToken={ MAPBOX_TOKEN } />
          {
            polygons.map((polygon, index) => {
              return (
                <Polygon { ...polygon.data }
                  key={`_polygon_${index}`} />
              )
            })
          }
          {
            polylines.map((polyline, index) => {
              return (
                <Polyline { ...polyline.data }
                  key={`_polylin_${index}`} />
              )
            })
          }
        </Map>
        { this.renderKits() }
      </div>
    )
  }

  renderKits () {
    const { selectedSource } = this.state
    
    console.log('selectedSource', selectedSource)

    
    return (
      <ul className='map-kits'>
        {
          KITS.map((item, index) => {
            return (
              <li className={classnames("map-kit-item", {
                  active: selectedSource.indexOf(item.name) > -1
                })}
                key={`_item_${index}`}
                onClick={ this.dataHandle.bind(this, item) }>
                { item.text }
              </li>
            )
          })
        }
      </ul>
    )
  }

  dataHandle (data) {
    console.log('dataHandle', data)
    let { name, type } = data
    let { selectedSource } = this.state

    if (selectedSource.indexOf(name) > -1) {
      selectedSource = selectedSource.filter((item) => {
        return item !== name
      })
    } else {
      selectedSource = selectedSource.concat([name])
    }

    this.setState({
      selectedSource: selectedSource
    })

    this.fetchData(type, name)
  }

}

export default MapView