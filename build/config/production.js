/**
 * production webpack config
 */
const webpack = require('webpack')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
  // webpack configure
  mode: 'production',
  cache: false,
  devtool: 'source-map',
  plugins: [
    new ExtractTextPlugin("[name].css"),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify('production') 
      }
    })
  ]
}