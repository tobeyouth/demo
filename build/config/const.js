const path = require('path')
const staticPath = path.resolve(__dirname, './../../static')

module.exports = {
  staticPath: staticPath,
  srcPath: path.resolve(staticPath, './src'),
  distPath: path.resolve(staticPath, './dist'),
  appPath: path.resolve(staticPath, './src/app.js'),
  serveFilePath: path.resolve(staticPath, './index.html')
}

